package com.greedy;

public class Pokemon {
	
	private String name;
	private int exp = 0;
	
	public Pokemon() {}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}
	
	
	
}

