package com.greedy;

public class Kkobugi extends Monster {
	public Kkobugi (int energy, int exp) {
		super(energy, exp);
	}
	@Override
	public void cry() {
		System.out.println("꼬부..............욱");
	}

	public void attactkKkobugi() {
		if(super.getEnergy() == 0 || super.getEnergy() < 0) {
			System.out.println("꼬부기가 쓰러집니다. " + super.getExp() + "만큼의 경험치를 얻었습니다.");
			cry();
		} else {
			System.out.println("꼬부기가 아파합니다, 에너지가 " + (int) ((Math.random() * 6) + 10) + "만큼 감소합니다.");
			super.setEnergy(super.getEnergy() - (int) (Math.random() * 6) + 10);
		}
	
		
	}
}

