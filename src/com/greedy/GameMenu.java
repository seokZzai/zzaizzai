package com.greedy;

import java.util.Scanner;

public class GameMenu {
	Scanner sc = new Scanner(System.in);

	Pokemon pokemon = new Pokemon();

	public void startGame(String name) {
		System.out.println(name + "을 선택하였습니다.");
		System.out.println("나와라!!! " + name);

		do {
			System.out.println("========  메인 메뉴 =======");
			System.out.println("1. 포켓몬 정보 확인");
			System.out.println("2. 사냥할 몬스터 선택");
			System.out.println("3. 종료하기");
			System.out.print("선택 : ");
			int no = sc.nextInt();

			int exp = 0;
			switch (no) {
			case 1:
				System.out.println("이름 : " + name + ", 공격력 : 10 - 15, 경험치 : " + pokemon.getExp());
				break;
			case 2:
				selectMonster();
				break;
			case 3:
				System.out.println("프로그램을 종료합니다.");
				return;
			default:
				System.out.println("잘못된 번호를 입력하셨습니다.");

			}

		} while (true);
	}

	public void selectMonster() {
		do {
			System.out.println("======= 사냥할 몬스터 선택 =======");
			System.out.println("1. 피카츄");
			System.out.println("2. 파이리");
			System.out.println("3. 꼬부기");
			System.out.println("4. 뒤로가기");
			System.out.print("선택 : ");
			int no = sc.nextInt();

			switch (no) {
			case 1:
				Monster pikachu = new Pikachu(100, 100);

				for (int i = pikachu.getEnergy(); i > 0; i -= (int) (Math.random() * 6) + 10) {
					if (pikachu instanceof Monster) {
						((Pikachu) pikachu).attackPikachu();
					}
				}
				pokemon.setExp(pokemon.getExp() + pikachu.getExp());

				break;
			case 2:
				Monster pairi = new Pairi(200, 200);

				for (int i = pairi.getEnergy(); i > 0; i -= (int) (Math.random() * 6) + 10) {
					if (pairi instanceof Monster) {
						((Pairi) pairi).attackPairi();
					}
				}
				pokemon.setExp(pokemon.getExp() + pairi.getExp());
				break;
			case 3:
				Monster kkobugi = new Kkobugi(400, 400);

				for (int i = kkobugi.getEnergy(); i > 0; i -= (int) (Math.random() * 6) + 10) {
					if (kkobugi instanceof Monster) {
						((Kkobugi) kkobugi).attactkKkobugi();
					}
					
				}
				pokemon.setExp(pokemon.getExp() + kkobugi.getExp());
				break;
			case 4:
				System.out.println("처음으로 돌아갑니다.");
				return;
			default:
				System.out.println("잘못된 번호를 입력하셨습니다.");
			}
		} while (true);
	}
}

