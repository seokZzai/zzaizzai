package com.greedy;

public class Monster {
	
	private int energy;
	private int exp;
	
	public Monster() {}
	
	public Monster(int energy, int exp) {
		this.energy = energy;
		this.exp = exp;
	}
	
	
	public int getEnergy() {
		return energy;
	}
	public void setEnergy(int energy) {
		this.energy = energy;
	}
	public int getExp() {
		return exp;
	}
	public void setExp(int exp) {
		this.exp = exp;
	}
	
	public void cry() {
		System.out.println("몬스터가 울음소리를 냅니다.");
	}
}

